import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router';  // named import
import routes from './config/routes';

if (typeof window !== 'undefined') {
	window.React = React;
}

ReactDOM.render(
		<Router>{routes}</Router>,
		document.getElementById('app')
);