# Gitub Note Taker #

React front-end for taking custom notes on github user accounts

* v0.1.0 - Pre-release alpha
* Uses Axios to push/pull from Github API
* Data persistence using Google Firebase API


### How do I get set up? ###

* Clone this repo
* Install Node modules ```npm install```
* Make sure you have `webpack` installed ```npm install -g webpack```
* Build JS app using webpack: ```webpack -w```
* Serve `/public` up with your favorite web server
* Open a browser to local address of `/public/index.html`
